# mon site 
Dolor numquam fuga optio voluptas nihil ipsam? A excepturi repellat architecto minima molestiae error Asperiores doloribus voluptates sit illum animi labore perspiciatis Temporibus totam laboriosam fugit beatae aliquam, quae quod sunt reiciendis neque? Rerum consectetur eos suscipit sequi tenetur assumenda ea quaerat ullam. Numquam veritatis reprehenderit aspernatur doloribus quisquam. Tempora laboriosam totam laudantium error mollitia Illo deserunt minima numquam voluptate nemo tenetur. Magni facere cumque provident sit iste? Repellat quia tempore ex praesentium maxime. Velit eos corrupti quas suscipit ab Fuga assumenda vitae voluptatem quo harum ratione? Id non qui maxime maxime esse labore temporibus. Qui reiciendis sit aliquam nostrum?
## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
